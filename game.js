/**
 * Created by pc on 7/29/2016.
 */
var nameHendler = document.getElementById("name");
//console.log(nameHendler);
nameHendler.innerHTML = " " + this.getCookie("username");


/**
 * 0 - white space
 * 1 - dist box
 * 2 - indist box
 * 3 - surprize box
 * -1 -player 1
 * -2 -player 2
 * -3 -player 3
 * -4 -player 4
 * @type {*[]}
 */
var map =
    [[2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
        [2, -1, 0, 0, 0, 0, 0, 0, 0, 2],
        [2, 0, 1, 1, 0, 0, 0, 0, 0, 2],
        [2, 0, 3, 1, 2, 2, 2, 2, 0, 2],
        [2, 0, 3, 1, 0, 1, 0, 0, 0, 2],
        [2, 0, 0, 0, 1, 3, 2, 0, 0, 2],
        [2, 0, 1, 1, 3, 1, 2, 0, 0, 2],
        [2, 0, 0, 1, 3, 0, 0, 0, 0, 2],
        [2, 2, 0, 0, 0, 3, 0, 0, 0, 2],
        [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]];

var player = new Object();
player.X = 1;
player.Y = 1;


mapRander();
function mapRander() {
    var table = document.getElementById("game-map");
    table.innerHTML = "";
    var z = 0;
    for (var i = 0; i < map[0].length; i++) {
        var row = document.createElement('tr');
        row.setAttribute('id', 'row-' + i);
        for (var j = 0; j < map.length; j++) {
            var col = document.createElement('td');
            col.setAttribute('id', 'col-' + z);
            switch (map[i][j]) {
                case -1:
                    var img = document.createElement('img');
                    img.setAttribute('src', 'images/player-1.png');
                    img.setAttribute('id', 'main_player');
                    col.appendChild(img);
                    row.appendChild(col);
                    break;
                case 0:
                    var img = document.createElement('img');
                    img.setAttribute('src', 'images/white.png');
                    col.appendChild(img);
                    row.appendChild(col);
                    break;
                case 1:
                    var img = document.createElement('img');
                    img.setAttribute('src', 'images/block.png');
                    col.appendChild(img);
                    row.appendChild(col);
                    break;
                case 2:
                    var img = document.createElement('img');
                    img.setAttribute('src', 'images/indestr.png');
                    col.appendChild(img);
                    row.appendChild(col);
                    break;
                case 3:
                    var img = document.createElement('img');
                    img.setAttribute('src', 'images/surprize-block.png');
                    col.appendChild(img);
                    row.appendChild(col);
                    break;
                default:
                    break;
            }
            z++;
        }
        table.appendChild(row);
    }
}

// Key events

document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;

    if (e.keyCode == '38') {
        //Up arrow
        console.log(map[player.Y][player.X]);
        if (map[player.Y][player.X] == -1 && player.Y > 1 && map[player.Y - 1][player.X] == 0) {
            map[player.Y][player.X] = 0;
            map[player.Y - 1][player.X] = -1;
            player.Y = player.Y - 1;
            mapRander();
        }
    }
    else if (e.keyCode == '40') {
        // down arrow
        if (map[player.Y][player.X] == -1 && player.Y < map[0].length - 1 && map[player.Y + 1][player.X] == 0) {
            map[player.Y][player.X] = 0;
            map[player.Y + 1][player.X] = -1;
            player.Y = player.Y + 1;
            mapRander();
        }
    }
    else if (e.keyCode == '37') {
        // left arrow
        if (map[player.Y][player.X] == -1 && player.X > 1 && map[player.Y][player.X - 1] == 0) {
            map[player.Y][player.X] = 0;
            map[player.Y][player.X - 1] = -1;
            player.X = player.X - 1;
            mapRander();
        }
    }
    else if (e.keyCode == '39') {
        // right arrow
        if (map[player.Y][player.X] == -1 && player.X < map.length - 1 && map[player.Y][player.X + 1] == 0) {
            map[player.Y][player.X] = 0;
            map[player.Y][player.X + 1] = -1;
            player.X = player.X + 1;
            mapRander();
        }
    }

}